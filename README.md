# ansible-users

Create and remove users

`global__users` is a dict of all users. You can specify a list of ssh keys in `ssh_keys` and the ansible groups where the user has access in the list `access`.
```yaml
global__users:
  example_user:
    access: [ 'servergroup1', 'servergroup2' ]
    ssh_keys:
    - "ssh-rsa [...]"
```
